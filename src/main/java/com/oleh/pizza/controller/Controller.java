package com.oleh.pizza.controller;

import com.oleh.pizza.model.bakery.Bakery;
import com.oleh.pizza.model.bakery.Pizza;
import com.oleh.pizza.model.bakery.dnipro.DniproBakery;
import com.oleh.pizza.model.bakery.kyiv.KyivBakery;
import com.oleh.pizza.model.bakery.lviv.LvivBakery;
import com.oleh.pizza.model.enums.City;
import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;

public class Controller implements ControllerInt{

    @Override
    public Pizza getPizza(City city, PizzaDought pizzaDought, PizzaSauce pizzaSauce, PizzaType pizzaType, String toppings) {
        Bakery bakery = null;
        switch (city){
            case KYIV:
                bakery = new KyivBakery();
                break;
            case LVIV:
                bakery = new LvivBakery();
                break;
            case DNIPRO:
                bakery = new DniproBakery();
                break;
        }
        if (bakery==null) {
            return null;
        }
        Pizza pizza = bakery.assemble(pizzaDought, pizzaSauce, pizzaType, toppings);
        return pizza;
    }
}
