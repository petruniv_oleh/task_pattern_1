package com.oleh.pizza.controller;

import com.oleh.pizza.model.bakery.Pizza;
import com.oleh.pizza.model.enums.City;
import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;

public interface ControllerInt {

    Pizza getPizza(City city, PizzaDought pizzaDought,
                   PizzaSauce pizzaSauce,
                   PizzaType pizzaType,
                   String toppings);
}
