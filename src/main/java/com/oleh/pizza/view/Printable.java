package com.oleh.pizza.view;

@FunctionalInterface
public interface Printable {
    void print() throws IllegalAccessException;
}
