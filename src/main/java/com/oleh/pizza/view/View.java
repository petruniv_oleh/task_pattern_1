package com.oleh.pizza.view;

import com.oleh.pizza.controller.Controller;
import com.oleh.pizza.model.bakery.Pizza;
import com.oleh.pizza.model.enums.City;
import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Order a pizza");
        menuMap.put("2", "2. Exit");


        menuMapMethods.put("1", this::orderPizza);


    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("2"));
    }


    @Override
    public void orderPizza() {
        logger.debug("Ordering a pizza");
        City city = getCity();
        if (city == null) {
            System.out.println("Wrong city!");
            return;
        }
        logger.debug("City = " + city);

        PizzaType pizzaType = getPizzaType();
        if (pizzaType == null) {
            System.out.println("Wrong pizza type!");
            return;
        }
        logger.debug("Pizza type = " + pizzaType);
        PizzaDought pizzaDought = getDought();
        if (pizzaDought == null) {
            System.out.println("Wron crust!");
            return;
        }
        logger.debug("Dought = " + pizzaDought);

        PizzaSauce pizzaSauce = getPizzaSauce();
        if (pizzaSauce == null) {
            System.out.println("Wrong sauce!");
            return;
        }
        logger.debug("Sauce ="+ pizzaSauce);
        String toppings = getToppings();
        logger.debug("Topings = " +toppings);

        Pizza pizza = controller.getPizza(city, pizzaDought, pizzaSauce, pizzaType, toppings);
        System.out.println("Get your pizza!");
        System.out.println(pizza);
    }

    private String getToppings() {
        System.out.println("Do you want to add some toppings (y/n)");
        if (scanner.nextLine().equals("n")) return null;
        else {
            System.out.println("Write all toppings you want to add: ");
            return scanner.nextLine();
        }

    }

    private PizzaSauce getPizzaSauce() {
        System.out.println("Choose sauce (MARINARA, PLUM TOMATO, PESTO, BBQ, SPECIAL)");
        String line = scanner.nextLine();
        line = line.toUpperCase();
        switch (line) {
            case "MARINARA":
                return PizzaSauce.MARINARA;
            case "PLUM TOMATO":
                return PizzaSauce.PLUM_TOMATO;
            case "PESTO":
                return PizzaSauce.PESTO;
            case "BBQ":
                return PizzaSauce.BBQ;
            case "SPECIAL":
                return PizzaSauce.SPECIAL;
            default:
                return null;
        }
    }

    private PizzaType getPizzaType() {
        System.out.println("Choose pizza type (CHEESE, VEGGIE, CLAM, PEPPERONI)");
        String line = scanner.nextLine();
        line = line.toLowerCase();
        switch (line) {
            case "cheese":
                return PizzaType.CHEESE;
            case "veggie":
                return PizzaType.VEGGIE;
            case "clam":
                return PizzaType.CLAM;
            case "pepperoni":
                return PizzaType.PEPPERONI;
            default:
                return null;
        }

    }

    private PizzaDought getDought() {
        System.out.println("Choose crust (thin or thick) ");
        String line = scanner.nextLine();
        line = line.toLowerCase();
        switch (line) {
            case "thin":
                return PizzaDought.THIN_CRUST;
            case "thick":
                return PizzaDought.THICK_CRUST;
            default:
                return null;

        }

    }

    public City getCity() {
        System.out.println("Choose your city (Lviv, Kyiv, Dnipro)");
        String line = scanner.nextLine();
        line = line.toLowerCase();
        switch (line) {
            case "lviv":
                return City.LVIV;
            case "dnipro":
                return City.DNIPRO;
            case "kyiv":
                return City.KYIV;
            default:
                return null;
        }
    }
}
