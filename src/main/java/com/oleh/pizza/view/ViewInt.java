package com.oleh.pizza.view;

public interface ViewInt {
    void showMenu();
    void orderPizza();
}
