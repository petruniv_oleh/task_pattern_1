package com.oleh.pizza.model.enums;

public enum PizzaSauce {
    MARINARA, PLUM_TOMATO, PESTO, BBQ, SPECIAL;
}
