package com.oleh.pizza.model.enums;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI;
}
