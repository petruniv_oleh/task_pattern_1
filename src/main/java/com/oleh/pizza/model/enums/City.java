package com.oleh.pizza.model.enums;

public enum City {
    LVIV, KYIV, DNIPRO;
}
