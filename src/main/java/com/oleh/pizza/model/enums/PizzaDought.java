package com.oleh.pizza.model.enums;

public enum  PizzaDought {
    THICK_CRUST, THIN_CRUST;
}
