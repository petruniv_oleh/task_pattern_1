package com.oleh.pizza.model.bakery.lviv;

import com.oleh.pizza.model.bakery.Bakery;
import com.oleh.pizza.model.bakery.Pizza;
import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;

public class LvivBakery extends Bakery {

    @Override
    public Pizza createPizza(PizzaDought pizzaDought, PizzaSauce pizzaSauce, PizzaType pizzaType, String toppings) {
        return new LvivPizza(pizzaDought, pizzaSauce, pizzaType, toppings);
    }
}
