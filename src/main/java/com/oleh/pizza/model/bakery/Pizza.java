package com.oleh.pizza.model.bakery;

import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;

public abstract class Pizza {
    private PizzaDought pizzaDought;
    private PizzaSauce pizzaSauce;
    private PizzaType pizzaType;
    private String toppings;

    public Pizza() {
    }

    public Pizza(PizzaDought pizzaDought, PizzaSauce pizzaSauce, PizzaType pizzaType, String toppings) {
        this.pizzaDought = pizzaDought;
        this.pizzaSauce = pizzaSauce;
        this.pizzaType = pizzaType;
        this.toppings = toppings;
    }

    public abstract void prepare();

    public void bake() {
        System.out.println("Baking pizza ...");
        System.out.println("Ready..");
    }


    public void cut() {
        System.out.println("Cutting pizza ...");
    }


    public void box() {
        System.out.println("Packing pizza...");
        System.out.println("Your order is ready");
    }

    public PizzaDought getPizzaDought() {
        return pizzaDought;
    }

    public void setPizzaDought(PizzaDought pizzaDought) {
        this.pizzaDought = pizzaDought;
    }

    public PizzaSauce getPizzaSauce() {
        return pizzaSauce;
    }

    public void setPizzaSauce(PizzaSauce pizzaSauce) {
        this.pizzaSauce = pizzaSauce;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getToppings() {
        return toppings;
    }

    public void setToppings(String toppings) {
        this.toppings = toppings;
    }

    @Override
    public String toString() {
        return "Your pizza{" +
                "pizzaDought=" + pizzaDought +
                ", pizzaSauce=" + pizzaSauce +
                ", pizzaType=" + pizzaType +
                ", toppings='" + toppings + '\'' +
                '}';
    }
}
