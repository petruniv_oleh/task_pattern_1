package com.oleh.pizza.model.bakery.lviv;

import com.oleh.pizza.model.bakery.Pizza;
import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;

public class LvivPizza extends Pizza {

    public LvivPizza(PizzaDought pizzaDought, PizzaSauce pizzaSauce, PizzaType pizzaType, String toppings) {
        super(pizzaDought, pizzaSauce, pizzaType, toppings);
    }

    @Override
    public void prepare() {
        System.out.println("Preparing pizza "+super.getPizzaType()+" in Lviv bakery!");
        System.out.println("Making "+super.getPizzaDought());
        System.out.println("Add "+super.getPizzaSauce()+" sauce");
        System.out.println("Add secret Lviv`s ingredient");
        if (super.getToppings()!= null){
            System.out.println("Add toppings: \n"+ super.getToppings());
        }
    }


}
