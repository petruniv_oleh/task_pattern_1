package com.oleh.pizza.model.bakery;

import com.oleh.pizza.model.enums.PizzaDought;
import com.oleh.pizza.model.enums.PizzaSauce;
import com.oleh.pizza.model.enums.PizzaType;

public abstract class Bakery {

    public abstract Pizza createPizza(PizzaDought pizzaDought,
                                      PizzaSauce pizzaSauce,
                                      PizzaType pizzaType,
                                      String toppings);

    public Pizza assemble(PizzaDought pizzaDought,
                          PizzaSauce pizzaSauce,
                          PizzaType pizzaType,
                          String toppings) {
        Pizza pizza = createPizza(pizzaDought, pizzaSauce, pizzaType, toppings);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

}
